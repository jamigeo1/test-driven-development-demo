import json
import re

regex_pattern = r'^93\d{3}$'

class Job_Application:
    
    @staticmethod
    def application(): 
        
        source_file = "companies.json"
        target_file = "sorted_companies.json"
        
        with open(source_file, "r", encoding="utf-8") as file:
            data = json.load(file)
            
        regex_companies = []
        

        for company in data["companies"]:
            for address in company["addresses"]:               
                if re.match(regex_pattern, str(address["postal_code"])):
                    regex_companies.append(company)
                    break
                
        sorted_companies = sorted(regex_companies, key=lambda x: x["id"])
        
        with open(target_file, "w", encoding="utf-8") as file:
            json.dump(sorted_companies, file, indent=4, ensure_ascii=False)
            
        result = sorted_companies
        
        print("Potentielle Firmen wurden in '{}' gespeichert.".format(target_file))
        return result

Job_Application.application()
