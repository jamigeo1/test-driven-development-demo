import unittest
import re
from application import Job_Application, regex_pattern

class TestJobApplicationAndDATA(unittest.TestCase):

    def test_application(self):
        job = Job_Application()  # Arrange

        result = job.application()  # Act

        for company in result:
            at_least_one_fit = False
            for address in company["addresses"]:
                if re.match(regex_pattern, str(address["postal_code"])) is not None:
                    at_least_one_fit = True
                    break

            self.assertTrue(at_least_one_fit)  # Assert

if __name__ == '__main__':
    unittest.main()
